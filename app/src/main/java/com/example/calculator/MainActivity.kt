package com.example.calculator

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.example.calculator.databinding.ActivityMainBinding
import com.example.calculator.fragments.ConstraintLayoutFragment
import com.example.calculator.fragments.FrameLayoutFragment
import com.example.calculator.fragments.LinearLayoutFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var isFragmentDisplayed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViews()
    }

    private fun initViews() {
        binding.apply {
            btnFrameLayoutCalculator.setOnClickListener {
                addFragment(FrameLayoutFragment())
            }

            btnLinearLayoutCalculator.setOnClickListener {
                addFragment(LinearLayoutFragment())
            }

            btnConstraintLayoutCalculator.setOnClickListener {
                addFragment(ConstraintLayoutFragment())
            }
        }
    }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager.commit {
            replace(binding.fragmentContainer.id, fragment)
            addToBackStack(null)
        }
        isFragmentDisplayed = true
        hideButtons()
    }

    private fun hideButtons() {
        binding.btnFrameLayoutCalculator.visibility = View.INVISIBLE
        binding.btnLinearLayoutCalculator.visibility = View.INVISIBLE
        binding.btnConstraintLayoutCalculator.visibility = View.INVISIBLE
    }

    private fun showButtons() {
        binding.btnFrameLayoutCalculator.visibility = View.VISIBLE
        binding.btnLinearLayoutCalculator.visibility = View.VISIBLE
        binding.btnConstraintLayoutCalculator.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        if (isFragmentDisplayed) {
            showButtons()
            supportFragmentManager.popBackStack()
            isFragmentDisplayed = false
        } else {
            finish()
        }
    }
}